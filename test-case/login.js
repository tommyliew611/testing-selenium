const {Builder, By, Key} = require("selenium-webdriver");

async function initial(){
    let driver = await new Builder().forBrowser("firefox").build();
    try{
        //launch the browser 
        //navigate to app
        await driver.get("https://aia-hk-aem.mayvenr.com/en/premieragency/index.html")
        // to do 
        await driver.findElement(By.className('loginMenu-btn')).click()
        await driver.findElement(By.id('username-placeholder')).sendKeys("A0001",Key.ENTER)
        await driver.findElement(By.id('password-placeholder')).sendKeys("A0001",Key.ENTER)
        await driver.findElement(By.id('staff-login-btn')).click()
    }catch(err){
        // quit the browser 
        await driver.quit()
    }

}

initial()